import string

def encodage(phrase_debut,choix_decalage):
	phrase_fin =""
	for char in phrase_debut:
		if(char !=" "):
			char = ord(char) + choix_decalage
			char = chr(char)
			phrase_fin = phrase_fin + char
		else:
			phrase_fin = phrase_fin + char
	return phrase_fin

def decodage(phrase_debut,choix_decalage):
	phrase_fin =""
	for char in phrase_debut:
		if(char !=" "):
			char = ord(char) - choix_decalage
			char = chr(char)
			phrase_fin = phrase_fin + char
		else:
			phrase_fin = phrase_fin + char
	return phrase_fin

def encodageCirculaire(phrase_debut,choix_decalage):
		phrase_fin = ""
		for char in phrase_debut:
			if char in string.ascii_lowercase:
				char = ord(char)
				char = (char - 97 + choix_decalage) %26 +97
				char = chr(char)
				phrase_fin = phrase_fin +char
			if char in string.ascii_uppercase:
				char = ord(char)
				char = (char -65 + choix_decalage) %26 +65
				char = chr(char)
				phrase_fin = phrase_fin + char
		return phrase_fin


def decodageCirculaire(phrase_debut,choix_decalage):
		phrase_fin = ""
		for char in phrase_debut:
			if char in string.ascii_lowercase:
				char = ord(char)
				char = (char - 97 - choix_decalage) %26 +97
				char = chr(char)
				phrase_fin = phrase_fin +char
			if char in string.ascii_uppercase:
				char = ord(char)
				char = (char -65 - choix_decalage) %26 +65
				char = chr(char)
				phrase_fin = phrase_fin + char
		return phrase_fin

import unittest
from cesar import encodage
from cesar import decodage 
from cesar import encodageCirculaire
from cesar import decodageCirculaire

    class TestMathsFunctions(unittest.TestCase):

    def test_encodage(self)
        self.assertEqual(encodage(abcd,8),ijkl)

    def test_decodage(self)
        self.assertEqual(decodage(ijkl,8),abcd)

    def test_decodageCirculaire(self)
        self.assertEqual(decodageCirculaire(abcd,11),klmn)

    def test_encodageCirculaire(self)
        self.assertEqual(encodageCirculaire(klmn,11),abcd)
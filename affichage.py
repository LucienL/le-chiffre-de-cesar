from tkinter import *
from cesar import encodage
from cesar import decodage
from cesar import encodageCirculaire
from cesar import decodageCirculaire

def maj_label_encodage():
    label_final.config(text="%s" % encodage(message.get(),nb_decalage.get()))

def maj_label_decodage():
    label_final.config(text="%s" % decodage(message.get(),nb_decalage.get()))

def maj_label_encodageCirculaire():
    label_final.config(text="%s" % encodageCirculaire(message.get(),nb_decalage.get()))

def maj_label_decodageCirculaire():
    label_final.config(text="%s" % decodageCirculaire(message.get(), nb_decalage.get()))

root = Tk()
root.title('Chiffrement de cesar')

root.rowconfigure(6, weight=2)
root.columnconfigure(6, weight=2)

qb = Button(root, text='Coder', command=maj_label_encodage)
qb.grid(row=1, column=2)

qb = Button(root, text='Decoder', command=maj_label_decodage)
qb.grid(row=1, column=3)

qb=Button(root, text='Coder circulairement', command=maj_label_encodageCirculaire)
qb.grid(row=2, column=2)

qb=Button(root, text='Decoder circulairement', command=maj_label_decodageCirculaire)
qb.grid(row=2, column=3)

qb = Button(root, text='Quitter', command=root.quit)
qb.grid(row=3, column=3)

message = StringVar()
message.set(' ')

saisi = Entry(root, textvariable=message)
saisi.grid(row=1, column=1)

label_final = Label(root, text='message à coder')
label_final.grid(row=3, column= 1)

nb_decalage = IntVar()
nb_decalage.set(8) 

choix_nb_decalage = Entry(root, textvariable=nb_decalage)
choix_nb_decalage.grid(row=2, column= 1)



root.mainloop()